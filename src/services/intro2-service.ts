import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './app-settings'
import { LoadingService } from './loading-service'

@Injectable()
export class Intro2Service {

    constructor(public af: AngularFireDatabase, private loadingService: LoadingService) {}

    getData = (): any => {

       return {
                  "btnFinish" : "Finish",
                  "btnNext" : "Next",
                  "items" : [ {
                    "backgroundImage" : "assets/images/background/9.jpg",
                    "description" : "Get blazing fast chargers at a location near you.",
                    "title" : "Welcome to PowerShare",
                    "welcome" : ""
                  }, {
                    "backgroundImage" : "assets/images/background/8.jpg",
                    "description" : "Now you can play games, use GPS, download data; all without giving a thought.  We've got your covered right when you need us.",
                    "title" : "For power hungry users",
                    "welcome" : ""
                  }, {
                    "backgroundImage" : "assets/images/background/10.jpg",
                    "description" : "Get the right rental plans that fit yor needs.",
                    "title" : "Easy rental options",
                    "welcome" : ""
                  } ]
               }; 
    }

    load(): Observable<any>{
        var that = this;
        that.loadingService.show();
        if (AppSettings.IS_FIREBASE_ENABLED) {
            return new Observable(observer => {
                this.af
                    .object('intro')
                    .valueChanges()
                    .subscribe(snapshot => {
                        that.loadingService.hide();
                        observer.next(snapshot);
                        observer.complete();
                    }, err => {
                        that.loadingService.hide();
                        observer.error([]);
                        observer.complete();
                    });
            });
        } else {
            return new Observable(observer => {
                that.loadingService.hide();
                observer.next(this.getData());
                observer.complete();
            });
        }
    };
}
