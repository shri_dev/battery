import { IService } from './IService';
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './app-settings'
import { LoadingService } from './loading-service'

@Injectable()
export class MenuService implements IService {

  constructor(public af: AngularFireDatabase, private loadingService: LoadingService) {}

    getId = ():string => 'menu';

    getTitle = ():string => 'UIAppTemplate';

    getAllThemes = (): Array<any> => {

      if(localStorage.getItem("isLoggedIn") == 'true'){
        return [
          {"title" : "Home", "theme"  : "nearbymaps",  "icon" : "ion-ios-home-outline", "listView" : false, "component": "", "singlePage":false},
          // {"title" : "Rent Battery", "theme"  : "rent_battery",  "icon" : "icon-flash", "listView" : false, "component": "", "singlePage":false},
          // {"title" : "Return Battery", "theme"  : "return_battery",  "icon" : "icon-battery", "listView" : false, "component": "", "singlePage":false},          
          {"title" : "History", "theme"  : "transactions",  "icon" : "ion-ios-time-outline", "listView" : true, "component":"", "singlePage":false},
          {"title" : "My Profile", "theme"  : "profile",  "icon" : "ion-ios-person-outline", "listView" : false, "component":"", "singlePage":false},
          {"title" : "Terms & Conditions", "theme"  : "t&c",  "icon" : "icon-dots-horizontal", "listView" : false, "component":"", "singlePage":false},
          {"title" : "Logout", "theme"  : "logout",  "icon" : "icon-logout", "listView" : false, "component":"", "singlePage":false}
        ];
      }else{
        return [
          {"title" : "Home", "theme"  : "nearbymaps",  "icon" : "ion-ios-home-outline", "listView" : false, "component": "", "singlePage":false},
          {"title" : "Rent Battery", "theme"  : "rent_battery",  "icon" : "icon-flash", "listView" : false, "component": "", "singlePage":false},
          {"title" : "Return Battery", "theme"  : "return_battery",  "icon" : "icon-battery", "listView" : false, "component": "", "singlePage":false},          
          {"title" : "Terms & Conditions", "theme"  : "t&c",  "icon" : "icon-dots-horizontal", "listView" : false, "component":"", "singlePage":false},
          {"title" : "Login", "theme"  : "login",  "icon" : "icon-login", "listView" : false, "component":"", "singlePage":false}
        ];
      }
    };

    getDataForTheme = () => {
      return{
        "background": "assets/images/background/14.jpg",
        "image": "assets/images/logo/1.png",
        "title": "PowerShare",
        "description": "Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor incididunt"
      };
    };

    getEventsForTheme = (menuItem: any): any => {
      return {};
    };

    prepareParams = (item: any) => {
      return {
        title: item.title,
        data: {},
        events: this.getEventsForTheme(item)
      };
    };

    load(item: any): Observable<any> {
      var that = this;
      that.loadingService.show();
      if (AppSettings.IS_FIREBASE_ENABLED) {
        return new Observable(observer => {
          this.af
            .object('menu')
            .valueChanges()
            .subscribe(snapshot => {
              that.loadingService.hide();
              observer.next(snapshot);
              observer.complete();
            }, err => {
              that.loadingService.hide();
              observer.error([]);
              observer.complete();
            });
        });
      } else {
        return new Observable(observer => {
          that.loadingService.hide();
          observer.next(this.getDataForTheme());
          observer.complete();
        });
      }
    }
}
