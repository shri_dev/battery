import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './app-settings'
import { LoadingService } from './loading-service'

@Injectable()
export class IntroService {

    constructor(public af: AngularFireDatabase, private loadingService: LoadingService) {}

    getData = (): any => {
        return {
            "btnPrev": "Previous",
            "btnNext": "Next",
            "btnFinish": "Finish",
            "items": [
             {
                 "logo": "assets/images/logo/2.png",
                 "title": "Welcome to PowerShare",
                 "description": "Get blazing fast chargers at a location near you."
             },
             {
                 "logo": "assets/images/logo/2.png",
                 "title": "For power hungry users",
                 "description": "Now you can play games, use GPS, download data; all without giving a thought.  We've got your covered right when you need us."
             },
             {
                 "logo": "assets/images/logo/2.png",
                 "title": "Easy rental options",
                 "description": "Get the right rental plans that fit yor needs."
             }
         ]
        };
    }

    load(): Observable<any>{
        var that = this;
        that.loadingService.show();
        if (AppSettings.IS_FIREBASE_ENABLED) {
            return new Observable(observer => {
                this.af
                    .object('intro')
                    .valueChanges()
                    .subscribe(snapshot => {
                        that.loadingService.hide();
                        observer.next(snapshot);
                        observer.complete();
                    }, err => {
                        that.loadingService.hide();
                        observer.error([]);
                        observer.complete();
                    });
            });
        } else {
            return new Observable(observer => {
                that.loadingService.hide();
                observer.next(this.getData());
                observer.complete();
            });
        }
    };
}
