import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalDetailsOldPage } from './rental-details-old';

@NgModule({
  declarations: [
    RentalDetailsOldPage,
  ],
  imports: [
    IonicPageModule.forChild(RentalDetailsOldPage),
  ],
})
export class RentalDetailsOldPageModule {}
