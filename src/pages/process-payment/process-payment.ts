import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the ProcessPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-process-payment',
  templateUrl: 'process-payment.html',
})
export class ProcessPaymentPage {
  
  constructor(public navCtrl: NavController,
  	public navParams: NavParams,
  	public loadingCtrl: LoadingController,
  	private alertCtrl: AlertController) {

  	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ProcessPaymentPage');
		this.process();
	}

	process(){
	    let loader = this.loadingCtrl.create({
	      content: "Redirecting to payment gateway.  Please wait...",
	      duration: 3000
	    });
	    loader.present();
	    setTimeout(() => {
		let alert = this.alertCtrl.create({
			      title: 'Success',
			      subTitle: 'Your payment has been completed.  Thank you!',
			      buttons: ['OK']
			    });
			    alert.present();
			    this.navCtrl.pop();
		}, 3200);
	}

}
