import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProcessPaymentPage } from './process-payment';

@NgModule({
  declarations: [
    ProcessPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(ProcessPaymentPage),
  ],
})
export class ProcessPaymentPageModule {}
