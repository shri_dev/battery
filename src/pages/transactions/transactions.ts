import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastService } from '../../services/toast-service';

/**
 * Generated class for the TransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transactions',
  templateUrl: 'transactions.html',
  providers: [ToastService]
})
export class TransactionsPage {

  params: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastService) {

   this.params.data = {
        "button" : "Ok",
        "header" : "",
        "items" : [ {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 1,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, KUL",
          "subtitle2" : " Pending",
          "textIcon" : "6 hours left",
          "title" : "Pebble - 10,000 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 2,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, KUL",
          "subtitle2" : "Pending",
          "textIcon" : "6 hours left",
          "title" : "Lapguard - 20800 mAH"
        }],
        "subtitle" : "You've 2 batteries awaiting replacement.",
        "title" : "Get Recharged !"
      };

	   this.params.events = {
            'onItemClick': function(item: any){
                  // toastCtrl.presentToast(item);
                  navCtrl.push('RentalDetailsPage', {
                    status1: 'Pending Return',
                    isDeposit: false
                  });
            },
            'onDelete': function(item: any){
                  // toastCtrl.presentToast("Delete " + item.title);
            },
            'onButtonGetClick': function(item: any){
                  // toastCtrl.presentToast("Like");
            }
        };
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad TransactionsPage');
  }

}
