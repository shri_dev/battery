import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalDetailsPage } from './rental-details';
import { QRcodeLayout1Module } from '../../components/qrcode/layout-1/qrcode-layout-1.module';

@NgModule({
  declarations: [
    RentalDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(RentalDetailsPage),
    QRcodeLayout1Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]  
})

export class RentalDetailsPageModule {}
