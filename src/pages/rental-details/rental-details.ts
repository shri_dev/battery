import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { IService } from '../../services/IService';
import { QRCodeService } from '../../services/qrcode-service';
import { Input, ViewChild } from '@angular/core';

/**
 * Generated class for the RentalDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-details',
  templateUrl: 'rental-details.html',
  providers: [QRCodeService]
})
export class RentalDetailsPage {
  item: any;
  status: any;
  isDeposit: any;
  timeLeft : any;
  cardNumber : any;
  page: any = {};
  service: IService;
  params: any = {};	

  constructor(public navCtrl: NavController,
  	public navParams: NavParams,
  	private qrContrl: QRCodeService,
  	private barcodeScanner: BarcodeScanner) {
    this.isDeposit = navParams.get('isDeposit');
    this.status = navParams.get('status1');
    this.cardNumber = '5428 6584 0643 4938';
  	this.item = navParams.get("item");
    this.page.title = 'Rent Battery';
    this.page.theme = 'layout1';

    this.service = qrContrl;
    if (this.service) {
      this.params = this.service.prepareParams(this.page, navCtrl);
      // this.scanner();
    } else {
      navCtrl.setRoot("HomePage");
    }
    console.log('rental details..');
    console.log(this.item);
  }

	scanner() {
	    this.barcodeScanner.scan()
	      .then((result) => {
	        this.params.data = result;
	      })
	      .catch((error) => {
	        alert(error);
	      })
	}

  addCard(){
    console.log('add card');
    this.navCtrl.push('AddCardPage');
  }

  processPayment(){
    console.log('payment process..');
    this.status = 'Payment Complete';
    this.navCtrl.push('ProcessPaymentPage');
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad RentalDetailsPage');
  }

}
