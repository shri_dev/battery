import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentBatteryPage } from './rent-battery';
import { QRcodeLayout1Module } from '../../components/qrcode/layout-1/qrcode-layout-1.module';

@NgModule({
  declarations: [
    RentBatteryPage
  ],
  imports: [
    IonicPageModule.forChild(RentBatteryPage),
    QRcodeLayout1Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class RentBatteryPageModule {}