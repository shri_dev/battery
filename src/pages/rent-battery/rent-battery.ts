import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { IService } from '../../services/IService';
import { QRCodeService } from '../../services/qrcode-service';


/**
 * Generated class for the RentBatteryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rent-battery',
  templateUrl: 'rent-battery.html',
  providers: [QRCodeService]
})

export class RentBatteryPage{

  page: any = {};
  service: IService;
  params: any = {};	

  constructor(public navCtrl: NavController,
  	public navParams: NavParams,
  	private qrContrl: QRCodeService,
  	private barcodeScanner: BarcodeScanner) {
  	
    this.page.title = 'Rent Battery';
    this.page.theme = 'layout1';

    this.service = qrContrl;
    if (this.service) {
      this.params = this.service.prepareParams(this.page, navCtrl);
      this.scanner();
    } else {
      navCtrl.setRoot("HomePage");
    }
  }

	scanner() {
	    this.barcodeScanner.scan()
	      .then((result) => {
	        this.params.data = result;
	      })
	      .catch((error) => {
	        alert(error);
	      })
	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RentBatteryPage');
  }

}
