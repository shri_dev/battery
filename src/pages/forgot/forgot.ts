import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastService } from '../../services/toast-service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
  providers: [ToastService]
})

export class ForgotPage{

  params: any = {};


  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastService) {
    this.params.data = {
      "forgotPassword" : "Forgot password?",
      "labelPassword" : "PASSWORD",
      "labelUsername" : "USERNAME",
      "login" : "Send Password Reset Link",
      "logo" : "assets/images/logo/2.png",
      "password" : "Enter your password",
      "register" : "Register",
      "skip" : "Skip",
      "subtitle" : "Welcome",
      "title" : "Recover your password",
      "username" : "Enter your username"
    };

   this.params.events = {
        onLogin: function(params) {
         console.log('onLogin');
         navCtrl.setRoot('HomePage');
        },
        onRegister: function(params){
            // console.log('onRegister');
            navCtrl.setRoot('RegisterPage');
        },
        onSkip: function(params){
            console.log('onSkip');
            navCtrl.setRoot('LoginPage');
        },
        onFacebook: function(params){
            console.log('onFacebook');
        },
        onTwitter: function(params) {
            console.log('onTwitter');
        },
        onGoogle: function(params) {
          console.log('onGoogle');
        },
        onPinterest: function(params) {
          console.log('onPinterest');
        }
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPage');
    this.toastCtrl.presentToast("Forgot password");
  }

}
