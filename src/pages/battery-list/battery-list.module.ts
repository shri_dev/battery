import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BatteryListPage } from './battery-list';
import { TabsLayout1Module } from '../../components/tabs/layout-1/tabs-layout-1.module';

@NgModule({
  declarations: [
    BatteryListPage,
  ],
  imports: [
    IonicPageModule.forChild(BatteryListPage),
	TabsLayout1Module

  ],
 schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class BatteryListPageModule {}