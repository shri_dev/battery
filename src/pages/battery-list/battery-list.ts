import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastService } from '../../services/toast-service';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the BatteryListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-battery-list',
  templateUrl: 'battery-list.html',
  providers: [ToastService]  
})

export class BatteryListPage {

  params: any = {};
  detailsPage : any;

  items = [
    'Power Bank - 20,800 mAH',
    'Power Bank - 4500 mAH',
    'Power Bank - 40,000 mAH',
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastService, public alertCtrl: AlertController) {
	   this.params.data = [
	        { page: "TransactionsPage", icon: "ios-timer-outline", title: "Current Rentals" },
	        { page: "PastRentalsPage", icon: "ios-list-box-outline", title: "Past Rentals" }
	   ];
	  this.params.events = {
	      'onItemClick': function (item: any) {
	            console.log("onItemClick");
	      }
	  };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BatteryListPage');
  }

  itemSelected(item: string) {
    // this.toastCtrl.presentToast("Return request initiated.");
    console.log(item);
    this.navCtrl.push("RentalDetailsPage", {item: item});
    return;
	  // let alert = this.alertCtrl.create({
	  //   title: 'Confirm Return',
	  //   message: 'Do you want return this battery?',
	  //   buttons: [
	  //     {
	  //       text: 'Cancel',
	  //       role: 'cancel',
	  //       handler: () => {
	  //         console.log('Cancel');
	  //       }
	  //     },
	  //     {
	  //       text: 'Yes',
	  //       handler: () => {
	  //             let alert = this.alertCtrl.create({
			// 	      title: 'Return Request Initiated',
			// 	      subTitle: 'Kindly wait until the merchant validates your return request.',
			// 	      buttons: ['OK']
			// 	  });
			//     alert.present();
	  //       }
	  //     }
	  //   ]
	  // });
	  // alert.present();
  }

}
