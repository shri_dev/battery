import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import {ParallaxLayout2Module} from '../../components/parallax/layout-2/parallax-layout-2.module';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    ParallaxLayout2Module
  ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfilePageModule {}
