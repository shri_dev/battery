import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  params: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
	   this.params.data = {
	      "avatar" : "assets/images/avatar/3.jpg",
	      "headerImage" : "assets/images/background-small/1.jpg",
	      "headerTitle" : "Profile",
	      "items" : [ {
	        "avatar" : "assets/images/avatar/2.jpg",
	        "button" : "Follow",
	        "id" : 2,
	        "title" : "Email",
	        "subtitle" : "zhangwei@gmail.com"
	      }, {
	        "avatar" : "assets/images/avatar/3.jpg",
	        "button" : "Follow",
	        "id" : 3,
	        "title" : "Contact",
	        "subtitle" : "+60 012 696 969"
	      }, {
	        "avatar" : "assets/images/avatar/4.jpg",
	        "button" : "Follow",
	        "id" : 4,
	        "title" : "Address",
	        "subtitle" : "Jalan Ampang, 50450 Kuala Lumpur, Malaysia. G.P.O. Box 22395"
	      }, {
	        "avatar" : "assets/images/avatar/4.jpg",
	        "button" : "Follow",
	        "id" : 4,
	        "title" : "Location",
	        "subtitle" : "KUL, Malaysia"
	      }, {
	        "avatar" : "assets/images/avatar/4.jpg",
	        "button" : "Follow",
	        "id" : 4,
	        "title" : "Card Number",
	        "subtitle" : "5734 6838 2958 2734"
	      },{
	        "avatar" : "assets/images/avatar/1.jpg",
	        "button" : "Follow",
	        "id" : 1,
	        "title" : "Total Rentals",
	        "subtitle" : "64"
	      }],
	      "subtitle" : "Member since January 01, 2018.",
	      "title" : "Zhang Wei"
	    }

	   this.params.events = {
	        'onProceed': function (item: any) {
	            console.log("onProceed");
	        },
	        'onShare': function (item: any) {
	            console.log("onShare");
	        },
	        'onItemClick': function (item: any) {
	            console.log("onItemClick");
	        },
	    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
