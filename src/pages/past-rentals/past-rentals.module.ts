import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PastRentalsPage } from './past-rentals';
import { SwipeToDismissLayout1Module } from '../../components/list-view/swipe-to-dismiss/layout-1/swipe-to-dismiss-layout-1.module';


@NgModule({
  declarations: [
    PastRentalsPage,
  ],
  imports: [
    IonicPageModule.forChild(PastRentalsPage),
	SwipeToDismissLayout1Module
  ],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class PastRentalsPageModule {}
