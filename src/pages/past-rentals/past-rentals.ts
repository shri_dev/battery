import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PastRentalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-past-rentals',
  templateUrl: 'past-rentals.html',
})
export class PastRentalsPage {
  params: any = {};


  constructor(public navCtrl: NavController, public navParams: NavParams) {
   this.params.data = {
        "button" : "Ok",
        "header" : "You've been Up & Running..",
        "items" : [ {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 1,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, Kuala Lumpur",
          "subtitle2" : "Completed",
          "textIcon1" : "6 hours",
          "title" : "Pebble - 10,000 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 2,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "3 hours",
          "title" : "Lapguard - 20800 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 3,
          "reply" : "Reply",
          "subtitle" : "The Gardens Mall, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "8 Hours",
          "title" : "Tukzer Quick Charge - 6000 mAH"
        },{
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 1,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, Kuala Lumpur",
          "subtitle2" : "Completed",
          "textIcon1" : "6 hours",
          "title" : "Pebble - 10,000 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 2,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "3 hours",
          "title" : "Lapguard - 20800 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 3,
          "reply" : "Reply",
          "subtitle" : "The Gardens Mall, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "8 Hours",
          "title" : "Tukzer Quick Charge - 6000 mAH"
        },{
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 1,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, Kuala Lumpur",
          "subtitle2" : "Completed",
          "textIcon1" : "6 hours",
          "title" : "Pebble - 10,000 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 2,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "3 hours",
          "title" : "Lapguard - 20800 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 3,
          "reply" : "Reply",
          "subtitle" : "The Gardens Mall, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "8 Hours",
          "title" : "Tukzer Quick Charge - 6000 mAH"
        },{
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 1,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, Kuala Lumpur",
          "subtitle2" : "Completed",
          "textIcon1" : "6 hours",
          "title" : "Pebble - 10,000 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 2,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "3 hours",
          "title" : "Lapguard - 20800 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 3,
          "reply" : "Reply",
          "subtitle" : "The Gardens Mall, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "8 Hours",
          "title" : "Tukzer Quick Charge - 6000 mAH"
        },{
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 1,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, Kuala Lumpur",
          "subtitle2" : "Completed",
          "textIcon1" : "6 hours",
          "title" : "Pebble - 10,000 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 2,
          "reply" : "Reply",
          "subtitle" : "JATOMI Fitness, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "3 hours",
          "title" : "Lapguard - 20800 mAH"
        }, {
          "delate" : "Delete",
          "icon" : "ios-information-circle-outline",
          "id" : 3,
          "reply" : "Reply",
          "subtitle" : "The Gardens Mall, KUL",
          "subtitle2" : "Completed",
          "textIcon1" : "8 Hours",
          "title" : "Tukzer Quick Charge - 6000 mAH"
        } ],
        "subtitle" : "We've kept you charged for 96 hours so far",
        "title" : "It's Amazing!"
      };

	   this.params.events = {
            'onItemClick': function(item: any){
                  // toastCtrl.presentToast(item);
                  navCtrl.push("RentalDetailsOldPage", {item: item, status:"completed"});
            },
            'onDelete': function(item: any){
                  // toastCtrl.presentToast("Delete " + item.title);
            },
            'onButtonGetClick': function(item: any){
                  // toastCtrl.presentToast("Like");
            }
        };  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PastRentalsPage');
  }

}
