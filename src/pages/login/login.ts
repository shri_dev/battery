import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage{

  params: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {

   this.params.data = {
      "forgotPassword" : "Forgot password?",
      "labelPassword" : "PASSWORD",
      "labelUsername" : "USERNAME",
      "login" : "Login",
      "logo" : "assets/images/logo/2.png",
      "password" : "Enter your password",
      "register" : "Register",
      "skip" : "Skip",
      "subtitle" : "Welcome",
      "title" : "Login to your account",
      "username" : "Enter your username"
   }
   this.params.events = {
        onLogin: function(params) {
         console.log('onLogin');
         events.publish('user:loggedin', Date.now());
        },
        onForgot: function() {
            console.log('onForgot');
            navCtrl.setRoot('ForgotPage');
        },
        onRegister: function(params){
            console.log('onRegister');
            navCtrl.setRoot('RegisterPage');
        },
        onSkip: function(params){
            console.log('onSkip');
            navCtrl.push("HomePage");
        },
        onFacebook: function(params){
            console.log('onFacebook');
        },
        onTwitter: function(params) {
            console.log('onTwitter');
        },
        onGoogle: function(params) {
          console.log('onGoogle');
        },
        onPinterest: function(params) {
          console.log('onPinterest');
        }
    };      	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
