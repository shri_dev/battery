import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NeatPage } from './neat';
import { MapsLayout3Module } from '../../components/maps/layout-3/maps-layout-3.module';


@NgModule({
  declarations: [
    NeatPage,
  ],
  imports: [
    IonicPageModule.forChild(NeatPage),
    MapsLayout3Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NeatPageModule {}
