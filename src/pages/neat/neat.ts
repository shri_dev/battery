import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NeatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-neat',
  templateUrl: 'neat.html',
})
export class NeatPage {
  params: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	   this.params.data = {
	      "headerTitle" : "Full Screen View",
	      "map" : {
	        "lat" : 40.712562,
	        "lng" : -74.005911,
	        "mapTypeControl" : true,
	        "streetViewControl" : true,
	        "zoom" : 15
	      },
	      "title" : "Your location"
	    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NeatPage');
  }

}
