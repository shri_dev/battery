import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

import { Intro2Service } from '../../services/intro2-service';

@IonicPage()
@Component({
  templateUrl: 'intro2-page.html',
  providers: [Intro2Service]
})
export class Intro2Page {
  params: any = null;

  constructor(public viewCtrl: ViewController, public introService: Intro2Service) {
    var that = this;

    this.introService.load().subscribe(snapshot => {
      setTimeout(function () {
        that.params = {
          'events': {
            'onFinish': function (event: any) {
              that.viewCtrl.dismiss();
            }
          },
          'data': snapshot
        };
      })
    });
  }
}
