import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Intro2Page } from './intro2-page';

import { WizardLayout2Module } from '../../components/wizard/layout-2/wizard-layout-2.module';

@NgModule({
    declarations: [
        Intro2Page,
    ],
    imports: [
        IonicPageModule.forChild(Intro2Page),
        WizardLayout2Module
    ],
    exports: [
        Intro2Page
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class Intro2PageModule { }