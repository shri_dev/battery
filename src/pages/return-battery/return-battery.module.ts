import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReturnBatteryPage } from './return-battery';
import { QRcodeLayout1Module } from '../../components/qrcode/layout-1/qrcode-layout-1.module';

@NgModule({
  declarations: [
    ReturnBatteryPage
  ],
  imports: [
    IonicPageModule.forChild(ReturnBatteryPage),
    QRcodeLayout1Module
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReturnBatteryPageModule {}