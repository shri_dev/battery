import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  params: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams) {
   this.params.data = {
      "city" : "Your home town",
      "country" : "Where are you from?",
      "email" : "Your e-mail address",
      "lableCity" : "CITY",
      "lableCountry" : "COUNTRY",
      "lableEmail" : "E-MAIL",
      "lablePassword" : "PASSWORD",
      "lableUsername" : "USERNAME",
      "logo" : "assets/images/logo/2.png",
      "password" : "Enter your password",
      "register" : "register",
      "skip" : "Skip",
      "title" : "Register your new account",
      "toolbarTitle" : "Register",
      "username" : "Enter your username"
    };
   this.params.events = {
            onRegister: function(params) {
                console.log('onRegister');
                navCtrl.setRoot('LoginPage');  
            },
            onSkip: function(params){
                console.log('onSkip');
                navCtrl.setRoot('LoginPage');
            }
        };      	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
