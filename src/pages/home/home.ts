import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { HomeService } from '../../services/home-service';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [HomeService]
})
export class HomePage {
  
  params: any = {};

  constructor(public navCtrl: NavController, public service:HomeService) {
    service.load().subscribe(snapshot => {
      // this.data = snapshot;
      this.params.data = {
          "headerTitle" : "Charging Points Near",
          "markers" : [
            {lat: -31.563910, lng: 147.154312},
            {lat: -33.718234, lng: 150.363181},
            {lat: -33.727111, lng: 150.371124}],
          "map" : {
            "lat" : 3.1390,
            "lng" : 101.6869,
            "mapTypeControl" : true,
            "streetViewControl" : true,
            "zoom" : 15
          },
          "map2" : {
            "lat" : 3.0738,
            "lng" : 101.5183,
            "mapTypeControl" : true,
            "streetViewControl" : true,
            "zoom" : 15
          },          
          "title" : "Kuala Lumpur"
        };
    });
  }

}
