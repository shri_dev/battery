import { Component, Input } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'forgot-layout-1',
    templateUrl: 'forgot.html'
})
export class ForgotLayout1 {
    @Input() data: any;
    @Input() events: any;

    public username: string;
    public password: string;

    constructor() { }

    onEvent = (event: string): void => {
        if (this.events[event]){
            this.events[event]({
                'username': this.username,
                'password': this.password
            });
        }
    }
}
