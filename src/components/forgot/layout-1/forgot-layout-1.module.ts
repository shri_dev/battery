import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgotLayout1 } from './forgot-layout-1';

@NgModule({
    declarations: [
        ForgotLayout1,
    ],
    imports: [
        IonicPageModule.forChild(ForgotLayout1),
    ],
    exports: [
        ForgotLayout1
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ForgotLayout1Module { }