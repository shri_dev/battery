import { Component, Input, ViewChild } from '@angular/core';
import { IonicPage, Content } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';



@IonicPage()
@Component({
    selector: 'maps-layout-3',
    templateUrl: 'maps.html'
})
export class MapsLayout3 {
    @Input() data: any;
    @Input() events: any;
    @ViewChild(Content)
    content: Content;

    active: boolean;

    constructor(public navCtrl: NavController, public events1: Events) {
        
    };

    onEvent(event: string) {
        if (this.events[event]) {
            this.events[event]();
        }
        console.log(event);
    }

    ngOnChanges(changes: { [propKey: string]: any }) {
        this.subscribeToIonScroll();
    }

    ngAfterViewInit() {
        this.subscribeToIonScroll();
    }

    ngAfterViewChecked() {
        this.subscribeToIonScroll();
    }

    isClassActive() {
        return this.active;
    }

    subscribeToIonScroll() {
        if (this.content != null && this.content.ionScroll != null) {
            this.content.ionScroll.subscribe((d) => {
                if (d.scrollTop < 200 ) {
                    this.active = false;
                    return;
                }
                this.active = true;
            });
        }
    }

  rentBattery(event) {
    console.log(event);
    // this.events1.publish('battery:rent', Date.now());
    if(localStorage.getItem("isLoggedIn") == null || localStorage.getItem("isLoggedIn") == "false"){
        this.navCtrl.setRoot('LoginPage');
    }else{
       this.navCtrl.push('RentBatteryPage');
    }
  }

  returnBattery(event){
    console.log(event);
    if(localStorage.getItem("isLoggedIn") == null || localStorage.getItem("isLoggedIn") == "false"){
       this.navCtrl.setRoot('LoginPage'); 
    }else{
       this.navCtrl.push('TransactionsPage');
    }
  }

}
