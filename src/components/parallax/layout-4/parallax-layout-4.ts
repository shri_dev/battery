import { Component, Input, ViewChild } from '@angular/core';
import { IonicPage, Content } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'parallax-layout-4',
  templateUrl: 'parallax.html'
})
export class ParallaxLayout4 {
  @Input() data: any;
  @Input() events: any;
  @ViewChild(Content)
  content: Content;

  headerImage:any = "";
  active: boolean;

  status : string = 'This battery is available for hire.';

  hideButton : boolean = false;

  date = Date();

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) { }

  onEvent(event: string, item: any, e: any) {
    if (e) {
      e.stopPropagation();
    }
    if (this.events[event]) {
      this.events[event](item);
    }
  }

  onStarClass(items: any, index: number, e: any) {
    for (var i = 0; i < items.length; i++) {
      items[i].isActive = i <= index;
    }
    this.onEvent("onRates", index, e);
  }

  ngOnChanges(changes: { [propKey: string]: any }) {
    if (changes.data && changes.data.currentValue) {
        this.headerImage = changes.data.currentValue.headerImage;
    } 
    this.subscribeToIonScroll();
  }

  ngAfterViewInit() {
    this.subscribeToIonScroll();
  }

  isClassActive() {
    return this.active;
  }

  subscribeToIonScroll() {
    if (this.content != null && this.content.ionScroll != null) {
      this.content.ionScroll.subscribe((d) => {
        if (d.scrollTop < 200) {
          this.active = false;
          return;
        }
        this.active = true;
      });
    }
  }

  initiateHireRequest(){
    console.log('Accepted');
    this.hideButton = true;
    this.status = 'Please wait for the merchant to approve rental.';
    // let alert = this.alertCtrl.create({
    //   title: 'Hire request initated!',
    //   subTitle: 'Kindly wait for the merchant to approve this rental.',
    //   buttons: ['OK']
    // });
    // alert.present();


      let loader = this.loadingCtrl.create({
        content: "Awaiting Approval.  Please wait...",
        duration: 6000
      });
      loader.present();
      setTimeout(() => {
        let alert = this.alertCtrl.create({
                title: 'Merchant Approved',
                subTitle: 'Please make the payment to begin rental',
                buttons: ['OK']
              });
              alert.present();
        }, 6200);
      this.status = '';
      this.navCtrl.push('RentalDetailsPage', {
        status1: 'Pending Payment',
        isDeposit: true
      });

  }

  deny(){
    console.log('Denied');
    this.status = 'This unit is available for hire';
    let alert = this.alertCtrl.create({
      title: 'Rental Denied!',
      subTitle: 'Rental to Zhang Wei is denied.',
      buttons: ['OK']
    });
    alert.present();
  }

}
