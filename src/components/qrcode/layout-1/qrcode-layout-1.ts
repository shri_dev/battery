import { Component, Input } from '@angular/core';
import { IonicPage } from 'ionic-angular';


@IonicPage()
@Component({
    selector: 'qrcode-layout-1',
    templateUrl: 'qrcode.html',
})

export class QRcodeLayout1 {
    @Input() data: any;
    @Input() events: any;

      params: any = {};

    constructor() {

        this.params.data = {
          "avatar" : "assets/images/background/battery.jpg",
          "category" : "Status : Active",
          "headerImage" : "assets/images/background/battery.jpg",
          "headerTitle" : "Battery Details",
          "items" : [ {
            "id" : 1,
            "subtitle" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            "title" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
          }, {
            "id" : 2,
            "subtitle" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            "title" : "Lorem ipsum dolor sit amet"
          }, {
            "id" : 3,
            "subtitle" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.",
            "title" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          }, {
            "id" : 4,
            "subtitle" : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
            "title" : "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
          } ],
          "shareIcon" : "more",
          "subtitle" : "This battery is available for hire.",
          "title" : "Tukzer Quick Charge"
        };

        this.params.events = {
            'onProceed': function (item: any) {
                console.log("onProceed");
            },
            'onShare': function (item: any) {
                console.log("onShare");
            },
            'onItemClick': function (item: any) {
                console.log("onItemClick");
            },
        };
    }


    onEvent(event: string, result:any) {
        if (this.events[event]) {
            this.events[event](result);
        }
    }
}
