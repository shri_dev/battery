import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QRcodeLayout1 } from './qrcode-layout-1';
import {ParallaxLayout4Module} from '../../parallax/layout-4/parallax-layout-4.module';

@NgModule({
    declarations: [
        QRcodeLayout1,
    ],
    imports: [
        IonicPageModule.forChild(QRcodeLayout1),
        ParallaxLayout4Module
    ],
    exports: [
        QRcodeLayout1
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class QRcodeLayout1Module { }
