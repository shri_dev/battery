import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuService } from '../services/menu-service';
import { AppSettings } from '../services/app-settings';
import { IService } from '../services/IService';
import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'app.html',
  providers: [MenuService]
})

export class MyApp{
  @ViewChild(Nav) nav: Nav;

  rootPage = "HomePage";
  pages: any;
  params:any;
  leftMenuTitle: string;

  constructor(public platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public menu: MenuController,
    private menuService: MenuService,
    public modalCtrl: ModalController,
    public events: Events) {

    localStorage.setItem("isLoggedIn", "false");

    events.subscribe('user:loggedin', (time) => {
        console.log('user logged in..');
        localStorage.setItem("isLoggedIn", "true");
        this.nav.setRoot('HomePage');
        this.pages = [
            {"title" : "Home", "theme"  : "nearbymaps",  "icon" : "ion-ios-home-outline", "listView" : false, "component": "", "singlePage":false},
            {"title" : "History", "theme"  : "transactions",  "icon" : "ion-ios-time-outline", "listView" : true, "component":"", "singlePage":false},
            {"title" : "My Profile", "theme"  : "profile",  "icon" : "ion-ios-person-outline", "listView" : false, "component":"", "singlePage":false},
            {"title" : "Terms & Conditions", "theme"  : "t&c",  "icon" : "icon-dots-horizontal", "listView" : false, "component":"", "singlePage":false},
            {"title" : "Logout", "theme"  : "logout",  "icon" : "icon-logout", "listView" : false, "component":"", "singlePage":false}
          ];
    });

    events.subscribe('user:loggedout', (time) => {
        console.log('user logged out..');
        localStorage.setItem("isLoggedIn", "false");
        this.nav.setRoot('LoginPage');
        this.pages = [
          {"title" : "Home", "theme"  : "nearbymaps",  "icon" : "ion-ios-home-outline", "listView" : false, "component": "", "singlePage":false},
          {"title" : "Terms & Conditions", "theme"  : "t&c",  "icon" : "icon-dots-horizontal", "listView" : false, "component":"", "singlePage":false},
          {"title" : "Login", "theme"  : "login",  "icon" : "icon-login", "listView" : false, "component":"", "singlePage":false}
        ];
    });

    this.initializeApp();

    this.pages = [
      {"title" : "Home", "theme"  : "nearbymaps",  "icon" : "ion-ios-home-outline", "listView" : false, "component": "", "singlePage":false},
      {"title" : "Terms & Conditions", "theme"  : "t&c",  "icon" : "icon-dots-horizontal", "listView" : false, "component":"", "singlePage":false},
      {"title" : "Login", "theme"  : "login",  "icon" : "icon-login", "listView" : false, "component":"", "singlePage":false}
    ];

    this.leftMenuTitle = menuService.getTitle();

    this.menuService.load(null).subscribe( snapshot => {
        this.params = snapshot;
    });

    if (localStorage.getItem("isWizardShown") == null && AppSettings.SHOW_START_WIZARD) {
      this.presentProfileModal();
      localStorage.setItem("isWizardShown", "true");
    }

  }



  initializeApp(){
    this.platform.ready().then(() => {
      this.nav.setRoot('HomePage');
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      localStorage.setItem("mailChimpLocal", "true");
    });
  }

  presentProfileModal() {
    const profileModal = this.modalCtrl.create("Intro2Page");
    profileModal.present();
  }

  openPage(page){
    // close the menu when clicking a link from the menu
    // navigate to the new page if it is not the current page
    if (page.singlePage){
        this.menu.open();
        this.nav.push(this.getPageForOpen(page.theme),{
          service: this.getServiceForPage(page.theme),
          page: page,
          componentName: page.theme
        });
    } else {
      console.log(' -- ');
      console.log('page theme = ' + page.theme);
      console.log('is logged in = '+ localStorage.getItem("isLoggedIn"));
      if(localStorage.getItem("isLoggedIn") == 'true'){
        if(page.theme == 'nearbymaps'){
          this.nav.setRoot('HomePage');
        }else if(page.theme == 'login'){
          this.nav.setRoot('LoginPage');
        }else if(page.theme == 'logout'){
          this.events.publish('user:loggedout', Date.now());
        }else if(page.theme == 'transactions'){
          this.nav.setRoot('PastRentalsPage');
        }else if(page.theme == 't&c'){
          this.nav.setRoot('TermsPage');
        }else if(page.theme == 'profile'){
          this.nav.setRoot('ProfilePage');
        }else if(page.theme == 'rent_battery'){
          this.nav.push('RentBatteryPage');
        }else if(page.theme == 'return_battery'){
          this.nav.push('BatteryListPage');
        }else{
          this.nav.setRoot("ItemsPage", {
            componentName: page.theme
          });
        }
      }else{
        if(page.theme == 'nearbymaps'){
          this.nav.setRoot('HomePage');
        }else if(page.theme == 'login'){
          this.nav.setRoot('LoginPage');
        }else if(page.theme == 't&c'){
          this.nav.setRoot('TermsPage');
        }else if(page.theme == 'rent_battery'){
          this.nav.setRoot('LoginPage');
        }else if(page.theme == 'return_battery'){
          this.nav.setRoot('LoginPage');
        }else if(page.theme == 'logout'){
          this.events.publish('user:loggedout', Date.now());
        }else{
          this.nav.setRoot('HomePage');
        }
      }
    }
  }

  getPageForOpen(value: string): any {
    return null;
  }

  getServiceForPage(value: string): IService {
    return null;
  }

}