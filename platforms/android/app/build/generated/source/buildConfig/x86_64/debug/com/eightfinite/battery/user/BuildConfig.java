/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.eightfinite.battery.user;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.eightfinite.battery.user";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "x86_64";
  public static final int VERSION_CODE = 35;
  public static final String VERSION_NAME = "0.0.3";
}
