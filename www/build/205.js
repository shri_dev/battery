webpackJsonp([205],{

/***/ 661:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RentalDetailsOldPageModule", function() { return RentalDetailsOldPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rental_details_old__ = __webpack_require__(966);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RentalDetailsOldPageModule = /** @class */ (function () {
    function RentalDetailsOldPageModule() {
    }
    RentalDetailsOldPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__rental_details_old__["a" /* RentalDetailsOldPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__rental_details_old__["a" /* RentalDetailsOldPage */]),
            ],
        })
    ], RentalDetailsOldPageModule);
    return RentalDetailsOldPageModule;
}());

//# sourceMappingURL=rental-details-old.module.js.map

/***/ }),

/***/ 966:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RentalDetailsOldPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RentalDetailsOldPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RentalDetailsOldPage = /** @class */ (function () {
    function RentalDetailsOldPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RentalDetailsOldPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RentalDetailsOldPage');
    };
    RentalDetailsOldPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-rental-details-old',template:/*ion-inline-start:"/Users/shri/Documents/Projects/Mark/BatteryRenting/renting/src/pages/rental-details-old/rental-details-old.html"*/'<!--\n  Generated template for the RentalDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Order Details</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-item-group>\n    <ion-item-divider color="light-green">Specifics</ion-item-divider>\n    \n\n    <ion-item>\n      <ion-icon name=\'ios-information-circle-outline\' item-start></ion-icon>\n      Status\n      <ion-note item-end>\n      Return Complete\n      </ion-note>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-icon name=\'ios-log-out-outline\' item-start></ion-icon>\n      Rent Out Time\n      <ion-note item-end>\n      Jan 16 @ 10:00 A.M.\n      </ion-note>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon name=\'ios-log-in-outline\' item-start></ion-icon>\n      Return Time\n      <ion-note item-end>\n      Jan 17 @ 12:00 P.M.\n      </ion-note>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon name=\'ios-pin-outline\' item-start></ion-icon>\n      Location\n      <ion-note item-end>\n      Jatomi Fitness, KUL\n      </ion-note>\n    </ion-item>\n\n  </ion-item-group>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/shri/Documents/Projects/Mark/BatteryRenting/renting/src/pages/rental-details-old/rental-details-old.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], RentalDetailsOldPage);
    return RentalDetailsOldPage;
}());

//# sourceMappingURL=rental-details-old.js.map

/***/ })

});
//# sourceMappingURL=205.js.map