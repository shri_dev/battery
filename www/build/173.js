webpackJsonp([173,218],{

/***/ 556:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsLayout1Module", function() { return TabsLayout1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_layout_1__ = __webpack_require__(832);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TabsLayout1Module = /** @class */ (function () {
    function TabsLayout1Module() {
    }
    TabsLayout1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tabs_layout_1__["a" /* TabsLayout1 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tabs_layout_1__["a" /* TabsLayout1 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__tabs_layout_1__["a" /* TabsLayout1 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], TabsLayout1Module);
    return TabsLayout1Module;
}());

//# sourceMappingURL=tabs-layout-1.module.js.map

/***/ }),

/***/ 621:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BatteryListPageModule", function() { return BatteryListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__battery_list__ = __webpack_require__(898);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_tabs_layout_1_tabs_layout_1_module__ = __webpack_require__(556);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var BatteryListPageModule = /** @class */ (function () {
    function BatteryListPageModule() {
    }
    BatteryListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__battery_list__["a" /* BatteryListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__battery_list__["a" /* BatteryListPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_tabs_layout_1_tabs_layout_1_module__["TabsLayout1Module"]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], BatteryListPageModule);
    return BatteryListPageModule;
}());

//# sourceMappingURL=battery-list.module.js.map

/***/ }),

/***/ 832:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TabsLayout1 = /** @class */ (function () {
    function TabsLayout1() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('data'),
        __metadata("design:type", Object)
    ], TabsLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('events'),
        __metadata("design:type", Object)
    ], TabsLayout1.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('tabs'),
        __metadata("design:type", Object)
    ], TabsLayout1.prototype, "tabRef", void 0);
    TabsLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'tabs-layout-1',template:/*ion-inline-start:"/Users/shri/Documents/Projects/Mark/BatteryRenting/renting/src/components/tabs/layout-1/tabs.html"*/'<!-- Theme Tabs Footer tab - text -->\n\n<ion-tabs #tabs tabsPlacement="bottom">\n\n    <ion-tab [tabTitle]="item.title" [tabIcon]="item.icon" [root]="item.page" *ngFor="let item of data;let i = index"></ion-tab>\n\n</ion-tabs>\n\n'/*ion-inline-end:"/Users/shri/Documents/Projects/Mark/BatteryRenting/renting/src/components/tabs/layout-1/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsLayout1);
    return TabsLayout1;
}());

//# sourceMappingURL=tabs-layout-1.js.map

/***/ }),

/***/ 898:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BatteryListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toast_service__ = __webpack_require__(341);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the BatteryListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BatteryListPage = /** @class */ (function () {
    function BatteryListPage(navCtrl, navParams, toastCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.params = {};
        this.items = [
            'Power Bank - 20,800 mAH',
            'Power Bank - 4500 mAH',
            'Power Bank - 40,000 mAH',
        ];
        this.params.data = [
            { page: "TransactionsPage", icon: "ios-timer-outline", title: "Current Rentals" },
            { page: "PastRentalsPage", icon: "ios-list-box-outline", title: "Past Rentals" }
        ];
        this.params.events = {
            'onItemClick': function (item) {
                console.log("onItemClick");
            }
        };
    }
    BatteryListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BatteryListPage');
    };
    BatteryListPage.prototype.itemSelected = function (item) {
        // this.toastCtrl.presentToast("Return request initiated.");
        console.log(item);
        this.navCtrl.push("RentalDetailsPage", {
            item: item,
            temp: 'tempValue'
        });
        return;
        // let alert = this.alertCtrl.create({
        //   title: 'Confirm Return',
        //   message: 'Do you want return this battery?',
        //   buttons: [
        //     {
        //       text: 'Cancel',
        //       role: 'cancel',
        //       handler: () => {
        //         console.log('Cancel');
        //       }
        //     },
        //     {
        //       text: 'Yes',
        //       handler: () => {
        //             let alert = this.alertCtrl.create({
        // 	      title: 'Return Request Initiated',
        // 	      subTitle: 'Kindly wait until the merchant validates your return request.',
        // 	      buttons: ['OK']
        // 	  });
        //     alert.present();
        //       }
        //     }
        //   ]
        // });
        // alert.present();
    };
    BatteryListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-battery-list',template:/*ion-inline-start:"/Users/shri/Documents/Projects/Mark/BatteryRenting/renting/src/pages/battery-list/battery-list.html"*/'<ion-header header-ios>\n  <ion-navbar >\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Hired Batteries</ion-title>\n</ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-item-group>\n    <ion-item-divider color="green">Current Rentals</ion-item-divider>\n              <ion-item border no-lines textIcon1">\n            <!-- Title -->\n            <h2 item-title>Test</h2>\n            <!-- Subtitle -->\n            <h3 item-subtitle>Hired from item.subtitle</h3>\n            <h3 item-subtitle>Return item.subtitle2</h3>\n            <!-- Details Item-->\n            <ion-note item-end>item.textIcon</ion-note>\n            <!-- Icon -->\n            <ion-icon item-end color="primary" name="item.icon"></ion-icon>\n          </ion-item>\n              <ion-item border no-lines textIcon1">\n            <!-- Title -->\n            <h2 item-title>item.title</h2>\n            <!-- Subtitle -->\n            <h3 item-subtitle>Hired from item.subtitle</h3>\n            <h3 item-subtitle>Return item.subtitle2</h3>\n            <!-- Details Item-->\n            <ion-note item-end>item.textIcon</ion-note>\n            <!-- Icon -->\n            <ion-icon item-end color="primary" name="item.icon"></ion-icon>\n          </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="green">Past Rentals</ion-item-divider>\n              <ion-item border no-lines textIcon1">\n            <!-- Title -->\n            <h2 item-title>item.title</h2>\n            <!-- Subtitle -->\n            <h3 item-subtitle>Hired from item.subtitle</h3>\n            <h3 item-subtitle>Return item.subtitle2</h3>\n            <!-- Details Item-->\n            <ion-note item-end>item.textIcon</ion-note>\n            <!-- Icon -->\n            <ion-icon item-end color="primary" name="item.icon"></ion-icon>\n          </ion-item>\n              <ion-item border no-lines textIcon1">\n            <!-- Title -->\n            <h2 item-title>item.title</h2>\n            <!-- Subtitle -->\n            <h3 item-subtitle>Hired from item.subtitle</h3>\n            <h3 item-subtitle>Return item.subtitle2</h3>\n            <!-- Details Item-->\n            <ion-note item-end>item.textIcon</ion-note>\n            <!-- Icon -->\n            <ion-icon item-end color="primary" name="item.icon"></ion-icon>\n          </ion-item>\n  </ion-item-group>\n</ion-content>\n\n\n<ion-list inset>\n  <button ion-item [navPush]="detailsPage" *ngFor="let item of items" (click)="itemSelected(item)">\n     item \n  </button>\n</ion-list>\n\n</ion-content>\n\n\n'/*ion-inline-end:"/Users/shri/Documents/Projects/Mark/BatteryRenting/renting/src/pages/battery-list/battery-list.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_toast_service__["a" /* ToastService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], BatteryListPage);
    return BatteryListPage;
}());

//# sourceMappingURL=battery-list.js.map

/***/ })

});
//# sourceMappingURL=173.js.map